﻿using System;
using System.Collections.Generic;

namespace ClassCalculate
{
    public class Calculater
    {
        static public double Calculate(List<Operation> operations, List<double> nums)
        {
            for (int i = 0; i < operations.Count; i++)
            {
                var operation = operations[i];
                nums[i + 1] = operation.Calculate(nums[i], nums[i + 1]);
            }
            return nums[nums.Count - 1];
        }
    }

    public abstract class Operation
    {
        public abstract double Calculate(double num1, double num2);
    }
    public class Plus : Operation
    {
        public override double Calculate(double num1, double num2)
        {
            return num1 + num2;
        }
    }

    public class Minus : Operation
    {
        public override double Calculate(double num1, double num2)
        {
            return num1 - num2;
        }
    }
    public class Division : Operation
    {
        public override double Calculate(double num1, double num2)
        {
            return num1 / num2;
        }
    }
    public class Multiplication : Operation
    {
        public override double Calculate(double num1, double num2)
        {
            return num1 * num2;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Calculater.Calculate(new List<Operation> { new Plus(), new Plus(), new Multiplication() }, new List<double> { 10, 20, 2, 2 }));

        }
    }
}
